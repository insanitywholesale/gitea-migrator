# TODOs
Things left to do until I can abandon this project

## Function structure
Improve functions by converting to taking arguments for configuration.
Also using that instead of having hardcoded env vars will help with testing

## Testing
Launch a dockerized gitea instance and try migrating some repos to it.
I started writing a testcontainers-based test but it's unfinished

## Authentication
Allow users to authenticate in different ways

### Token
While username and password are okay for ease of use, look into
[using the api](https://docs.gitea.io/en-us/api-usage/) to generate a token and use that too
