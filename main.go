package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"code.gitea.io/sdk/gitea"
)

type GithubRepo struct {
	FullName string `json:"full_name"`
	HTMLURL  string `json:"html_url"`
}

type GitlabRepo struct {
	PathWithNamespace string `json:"path_with_namespace"`
	WebURL            string `json:"web_url"`
}

func createClient() (*gitea.Client, error) {
	username := os.Getenv("GITEA_USERNAME")
	password := os.Getenv("GITEA_PASSWORD")
	if username == "" && password == "" {
		return nil, errors.New("a username and password is required for operation")
	} else if username == "" || password == "" {
		return nil, errors.New("both a username and password is required for operation")
	} else {
		url := os.Getenv("GITEA_URL")
		if url == "" {
			return nil, errors.New("a url for the gitea instance is required for operation")
		}
		giteaClient, err := gitea.NewClient(url, gitea.SetBasicAuth(username, password))
		if err != nil {
			return nil, err
		}
		return giteaClient, nil
	}
}

func getRepos(giteaClient *gitea.Client) []gitea.MigrateRepoOption {
	var repos []string

	repoEnv := os.Getenv("REPOS")
	ghName := os.Getenv("GITHUB_USERNAME")
	glName := os.Getenv("GITLAB_USERNAME")
	cbName := os.Getenv("CODEBERG_USERNAME")

	if repoEnv == "" && ghName == "" && glName == "" && cbName == "" {
		log.Fatalln("at least one repository link or GITHUB_USERNAME environment variable or GITLAB_USERNAME environment variable CODEBERG_USERNAME environment variable is required for operation")
	}

	if repoEnv == "" {
		log.Println("environment variable REPOS not set, automatic repo detection for github and gitlab will be tried")
	} else {
		repos = append(repos, strings.Split(repoEnv, ",")...)
	}
	if ghName == "" {
		log.Println("environment variable GITHUB_USERNAME not set, automatic repo detection for github won't be used")
	} else {
		log.Println("environment variable GITHUB_USERNAME has been set, automatic repo detection for gitlab starting now")
		res, err := http.Get("https://api.github.com/users/" + ghName + "/repos")
		if err != nil {
			log.Println("error getting repos of user " + ghName + ":" + err.Error())
		}
		defer res.Body.Close()
		body, err := io.ReadAll(res.Body)
		if err != nil {
			log.Println("error reading repos of user " + ghName + ":" + err.Error())
		}
		var ghRepos []GithubRepo
		err = json.Unmarshal(body, &ghRepos)
		if err != nil {
			log.Println("github json unmarshal error:", err)
		}
		log.Println(ghRepos)
		for _, ghRepo := range ghRepos {
			repos = append(repos, ghRepo.HTMLURL)
		}
	}
	if ghName == "" {
		log.Println("environment variable GITLAB_USERNAME not set, automatic repo detection for gitlab won't be used")
	} else {
		log.Println("environment variable GITLAB_USERNAME has been set, automatic repo detection for gitlab starting now")
		res, err := http.Get("https://gitlab.com/api/v4/users/" + glName + "/projects")
		if err != nil {
			log.Println("error getting repos of user " + glName + ":" + err.Error())
		}
		defer res.Body.Close()
		body, err := io.ReadAll(res.Body)
		if err != nil {
			log.Println("error reading repos of user " + glName + ":" + err.Error())
		}
		var glRepos []GitlabRepo
		err = json.Unmarshal(body, &glRepos)
		if err != nil {
			log.Println("gitlab json unmarshal error:", err)
		}
		log.Println(glRepos)
		for _, glRepo := range glRepos {
			repos = append(repos, glRepo.WebURL)
		}
		//TODO: add codeberg
	}

	var migrationOptions = []gitea.MigrateRepoOption{}
	for _, repo := range repos {
		if strings.HasPrefix(repo, "https://github.com") {
			migOpt := gitea.MigrateRepoOption{
				CloneAddr:      repo,
				RepoName:       strings.Split(repo, "/")[len(strings.Split(repo, "/"))-1],
				Service:        gitea.GitServiceGithub,
				DisableAuth:    true,
				Mirror:         true,
				MirrorInterval: "13h12m",
			}
			err := migOpt.Validate(giteaClient)
			if err != nil {
				log.Println("migration option oopsie: ", err)
			}
			migrationOptions = append(migrationOptions, migOpt)
		} else if strings.HasPrefix(repo, "https://gitlab.com") {
			migOpt := gitea.MigrateRepoOption{
				CloneAddr:      repo,
				RepoName:       strings.Split(repo, "/")[len(strings.Split(repo, "/"))-1],
				Service:        gitea.GitServiceGitlab,
				DisableAuth:    true,
				Mirror:         true,
				MirrorInterval: "13h12m",
			}
			err := migOpt.Validate(giteaClient)
			if err != nil {
				log.Println("migration option oopsie: ", err)
			}
			migrationOptions = append(migrationOptions, migOpt)
		} else if strings.HasPrefix(repo, "https://codeberg.org") {
			migOpt := gitea.MigrateRepoOption{
				CloneAddr:      repo,
				RepoName:       strings.Split(repo, "/")[len(strings.Split(repo, "/"))-1],
				Service:        gitea.GitServiceGitea,
				DisableAuth:    true,
				Mirror:         true,
				MirrorInterval: "13h12m",
			}
			err := migOpt.Validate(giteaClient)
			if err != nil {
				log.Println("migration option oopsie: ", err)
			}
			migrationOptions = append(migrationOptions, migOpt)
		} else {
			//TODO: fallback to plain git instead of erroring out
			log.Println("sorry," + repo + "can't be migrated, only github, gitlab and codeberg are supported")
		}
	}
	return migrationOptions
}

func migrate(giteaClient *gitea.Client, repos []gitea.MigrateRepoOption) (int, []error) {
	var migrationErrors []error
	var successfulMigrations int
	actuallyMigrate := os.Getenv("ACTUALLY_MIGRATE")
	if actuallyMigrate == "" {
		for _, repo := range repos {
			_, _, err := giteaClient.MigrateRepo(repo)
			if err != nil {
				migrationErrors = append(migrationErrors, err)
				log.Println("migration oopsie: ", err)
				successfulMigrations++
			}
		}
		return successfulMigrations, migrationErrors
	}
	return -1, nil
}

func main() {
	giteaClient, err := createClient()
	if err != nil {
		log.Fatalln("error creating gitea client:", err)
	}
	repos := getRepos(giteaClient)
	log.Println("repos:", repos)
	migOK, errs := migrate(giteaClient, repos)
	//TODO: this should probably be a check internal to migrate()
	if migOK < 0 {
		log.Println("not going to actually migrate")
		return
	}
	log.Println("migrations ", migOK, " out of ", len(repos), " went OK")
	if len(errs) > 0 {
		log.Println("errors encountered:")
		for err := range errs {
			log.Println(fmt.Errorf("%v", err))
		}
	}
}
