package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"

	"code.gitea.io/sdk/gitea"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait" // need to figure out custom waitfor port
)

type giteaContainer struct {
	testcontainers.Container
	URI  string
	Port string
}

var gport string

// TODO: need to find an automatic way of setting up an account and token
func setupGitea(ctx context.Context) (*giteaContainer, error) {
	req := testcontainers.ContainerRequest{
		Image:        "gitea/gitea:dev-rootless",
		ExposedPorts: []string{"3000/tcp"},
		WaitingFor:   wait.ForLog("Starting new Web server").WithPollInterval(1 * time.Second),
	}
	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, err
	}

	ip, err := container.Host(ctx)
	if err != nil {
		return nil, err
	}

	mappedPort, err := container.MappedPort(ctx, "3000")
	if err != nil {
		return nil, err
	}

	uri := fmt.Sprintf("http://%s:%s", ip, mappedPort.Port())
	log.Println("gitea URI:", uri)

	// Required because mappedPort is like 49294/tcp
	s := strings.Split(fmt.Sprintf("%s", mappedPort), "/")
	p := strings.Trim(s[0], " ")

	log.Println("gitea port:", p)

	gport = p

	return &giteaContainer{Container: container, URI: uri, Port: p}, nil
}

func TestIntegrationGiteaMigrationFlow(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	ctx := context.Background()

	giteaC, err := setupGitea(ctx)
	if err != nil {
		t.Fatal(err)
	}

	// Clean up the container after the test is complete
	defer func() {
		if err := giteaC.Terminate(ctx); err != nil {
			t.Fatal("failed to terminate container: %w", err)
		}
	}()

	resp, err := http.Get(giteaC.URI)
	if err != nil || resp.StatusCode != http.StatusOK {
		if resp != nil {
			t.Fatal(fmt.Sprintf("Expected status code %d. Got %d.", http.StatusOK, resp.StatusCode) + "Error: " + err.Error())
		}
		t.Fatal("http response was nil")
	}

	// Run migrations
	migrateCmd := []string{
		"gitea",
		"migrate",
	}
	migrateExitCode, r, err := giteaC.Exec(ctx, migrateCmd)
	if err != nil || migrateExitCode != 0 {
		output, _ := io.ReadAll(r)
		t.Fatal("Couldn't execute user creation command: " + string(output))
	}

	// Create admin user
	createUserCmd := []string{
		"gitea",
		"admin",
		"user",
		"create",
		"--username",
		"giteatester",
		"--password",
		"giteapasswd",
		"--email",
		"giteatester@example.com",
	}
	// Run create admin user command
	createUserExitCode, r, err := giteaC.Exec(ctx, createUserCmd)
	if err != nil || createUserExitCode != 0 {
		output, _ := io.ReadAll(r)
		t.Fatal("Couldn't execute user creation command: " + string(output))
	}

	// Set the URL
	url := "http://127.0.1.1" + ":" + giteaC.Port
	t.Log("gitea url:", url)

	//TODO: fix gitea client getting the wrong port in the url without deleting the version check from the SDK
	//NOTE: the version check is currently patched out in the vendored SDK
	gc, err := gitea.NewClient("http://127.0.2.1:"+giteaC.Port, gitea.SetBasicAuth("giteapasswd", "giteapasswd"), gitea.SetDebugMode())
	if err != nil && gc == nil {
		t.Fatal("Could not create gitea client: ", err)
	}
}

func TestCreateClient(t *testing.T) {
	// Configure gitea client using env vars
	os.Setenv("GITEA_USERNAME", "giteatester")
	os.Setenv("GITEA_PASSWORD", "giteapasswd")
	os.Setenv("GITEA_URL", "http://127.0.0.1:3000")

	// Run function to create client
	gc, err := createClient()
	if err != nil {
		t.Fatal(err)
	}
	if gc == nil {
		t.Fatal("gitea client is nil")
	}
}

func TestGetRepos(t *testing.T) {
	//t.Skip() // Skip it because it fails due to SDK problems

	// Create client configured using env vars
	os.Setenv("GITEA_USERNAME", "giteatester")
	os.Setenv("GITEA_PASSWORD", "giteapasswd")
	os.Setenv("GITEA_URL", "http://127.0.0.1:3000")
	gc, err := createClient()
	if err != nil {
		t.Fatal(err)
	}

	// Configure repos to be migrated using env vars
	os.Setenv("REPOS", "https://gitlab.com/insanitywholesale/go-grpc-microservice-template,https://github.com/insanitywholesale/adopse-2021")
	os.Setenv("GITHUB_USERNAME", "insanitywholesale")
	os.Setenv("GITLAB_USERNAME", "insanitywholesale")

	// Run the function to collect all the repos
	opts := getRepos(gc)
	if len(opts) < 4 {
		t.Fatal("expected repo opts to be at least 4 but they are ", len(opts))
	}
}

func TestMigrate(t *testing.T) {
	t.Skip() // Skip it because it fails due to connection refused(???)

	// Create client configured using env vars
	os.Setenv("GITEA_USERNAME", "giteatester")
	os.Setenv("GITEA_PASSWORD", "giteapasswd")
	os.Setenv("GITEA_URL", "http://localhost:3000")
	gc, err := createClient()
	if err != nil {
		t.Fatal(err)
	}

	// Configure repos to be migrated using env vars
	os.Setenv("REPOS", "https://gitlab.com/insanitywholesale/go-grpc-microservice-template,https://github.com/insanitywholesale/adopse-2021")
	os.Setenv("GITHUB_USERNAME", "insanitywholesale")
	os.Setenv("GITLAB_USERNAME", "insanitywholesale")

	// Run the function to collect all the repos
	repos := getRepos(gc)

	//TODO: figure out why it says connection refused
	// Run a mock migration (need to set ACTUALLY_MIGRATE to do it for real)
	migOK, errs := migrate(gc, repos)
	if migOK != len(repos) {
		t.Error("only", migOK, "repos out of", len(repos), "migrated successfully")
	}
	if len(errs) > 0 {
		t.Log(len(errs), "errors encountered:")
		for err := range errs {
			t.Log(fmt.Errorf("%v", err))
		}
		t.Fatal("not all repos could be successully migrated")
	}
}
