module gitlab.com/insanitywholesale/gitea-migrator

go 1.16

require (
	code.gitea.io/sdk/gitea v0.15.1
	github.com/hashicorp/go-version v1.6.0 // indirect
	github.com/testcontainers/testcontainers-go v0.15.0
)
